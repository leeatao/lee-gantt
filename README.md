# lee-gantt
#### 简介
lee-gantt是一个基于jquery的甘特图插件,根据对应的数据格式可以直接呈现基础的甘特图效果。
#### 地址预览
[在线预览](http://leeatao.gitee.io/lee-gantt/)
#### 使用方法
1.  下载插件包解压。
2.  引入对应的js文件和css文件。
```
<link href="css/lee-gantt.css" rel="stylesheet" />
```
```
<script src="js/jquery.min.js"></script>
<script src="js/lee-gantt.js"></script>
```
3. 给指定的div或者容器绑定插件即可调用，配置示例：
```
$("#gantt-box").gantt({
            title: 'xxx项目任务甘特图',
            leftWidth: 250,
            data: [{
                id: '1',
                title: '超级任务',
                desc: '任务备注啊',
                values: [
                    {
                        startDate: '2023-01-03',
                        endDate: '2023-01-12',
                        progress: 90,
                        label: '任务第一计划',
                        skin: 'danger'
                    }
                ]
            }]
        });
```
#### 属性说明
````
 var settings = {
            extendDays: 5,                  //前后延伸天数
            title: '',                      //甘特图标题
            weeks: ["日", "一", "二", "三", "四", "五", "六"],
            months: ["一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月"],
            leftWidth: 200,                 //左侧宽度
            boxSize:30,                     //任务数据格子尺寸，宽高值      
            data: [],                       //任务数据
            onItemClick: null,              //点击任务条回调函数
            onItemTitleClick: null,         //点击左侧标题回调函数
            onColumnClick: null,            //点击数据区日期格子回调函数
            onLoadFinish: null              //加载完成回调函数
        };
 ````

 ````
 data: [{
                id: '1',                        //任务id，保证唯一
                title: '超级任务',               //任务标题
                desc: '任务备注啊',              //任务备注，默认鼠标停留标题时显示
                values: [                       //任务值，可支持一个任务多个片段
                    {
                        startDate: '2023-01-03',//任务片段开始日期
                        endDate: '2023-01-12',  //任务片段结束日期
                        progress: 90,           //任务片段当前进度
                        label: '任务第一计划',   //任务片段标题
                        skin: 'danger'          //任务条显示样式,primary/warning/info/danger/success
                    }
                ]
            } 
````

#### 效果预览
具体查看示例页面
[在线预览](http://leeatao.gitee.io/lee-gantt/)
![输入图片说明](pv1.png)

#### 关于许可协议和更新
这个插件遵循MIT许可协议，也不一定会不会更新，大家喜欢直接拉走去用吧。。